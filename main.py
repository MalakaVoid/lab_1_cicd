import random
import sys

arr = [random.randint(0, 100) for i in range(100)]
arr.sort()

print(sys.argv)

input_value = int(sys.argv[1])

def binary_search(arr, num):
    low = 0
    high = len(arr) - 1
    while low <= high:
        mid = low + (high - low) // 2
        if arr[mid] == num:
            return mid
        elif arr[mid] < num:
            low = mid + 1
        else:
            high = mid - 1

    return -1


print(binary_search(arr, input_value))