FROM python:3

WORKDIR /app

COPY .. /app

ENTRYPOINT ["sh", "-c", "read -p 'Enter a number: ' my_varible && python3 main.py $my_varible"]